const { request, response } = require('express')
const express = require('express')
const db = require("../db")
const utils = require("../utils")
const router = express.Router()

router.post('/',(request,response)=>{
    const {name, salary, age} = request.body

    const statement = `
    insert into Emp
       (name, salary, age)
    values
       ('${name}','${salary}','${age}')   
    `

    db.execute(statement,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.get('/',(request,response)=>{
    const query = 'select * from Emp'

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.put('/:id',(request,response)=>{
    const {id} = request.params
    const {salary , age} = request.body

    const query = `
    update Emp
      set
    salary = '${salary}',
    age = '${age}'
      where empid='${id}'
    `
    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.delete('/:id',(request,response)=>{
    const {id} = request.params

    const query = `delete from Emp where empid='${id}'`

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

module.exports = router